#!/usr/bin/env bash
REQUIRED_LIB="make g++ libqt4-dev phonon-backend-vlc phonon cmake openbox libx11-dev libxdamage-dev libxxf86vm-dev libxrender-dev libxcomposite-dev libasound2-dev libphonon-dev gksu pcmanfm libqt4-sql-sqlite oxygen-icon-theme"
DATE=$(date +%Y%m%d%H%M)
ARCH=$(dpkg --print-architecture)
DEBIAN_REVISION=1
if [ -n "$1" ]; then
CONFIG_DIR=$1/.config
else
	CONFIG_DIR="${HOME}/.config"
fi
echo -n "${DATE}" > date.txt
echo -n "${ARCH}" > architecture.txt
if [ ! -d build ]; then
	mkdir build
fi
cd build
sudo apt-get -y install ${REQUIRED_LIB}
cmake ../
make package
sudo apt-get -y purge qtpanel
sudo dpkg -i qtpanel_${DATE}-${DEBIAN_REVISION}_${ARCH}.deb
cd ..
mkdir -v -p $CONFIG_DIR/madfish
if [ ! -e "${CONFIG_DIR}/madfish/qtpanel.conf" ]; then
	cp -v data/qtpanel.conf ${CONFIG_DIR}/madfish
fi
if [ ! -e "${CONFIG_DIR}/madfish/applets.xml" ]; then
	cp -v data/applets.xml ${CONFIG_DIR}/madfish
fi
if [ ! -f "$CONFIG_DIR/openbox/autostart" ]; then
	mkdir -p $CONFIG_DIR/openbox
	touch $CONFIG_DIR/openbox/autostart
fi
TEMP1=$(grep qtpanel ${CONFIG_DIR}/openbox/autostart)
TEMP2=$(grep pcmanfm ${CONFIG_DIR}/openbox/autostart)
if [ "$TEMP1" = "" ]; then
	echo "qtpanel &" >> $CONFIG_DIR/openbox/autostart
fi
if [ "$TEMP2" = "" ]; then
	echo "pcmanfm --desktop &" >> $CONFIG_DIR/openbox/autostart
fi
rm -R -f build
if [ "$1" = "-purge" ]; then
	sudo apt-get purge -y libqt4-dev cmake g++ libx11-dev libxxf86vm-dev libxrender-dev libxcomposite-dev libasound2-dev  libphonon-dev
	sudo apt-get -y --purge autoremove
fi
